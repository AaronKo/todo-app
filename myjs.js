/**
 * Created by Aaron on 4/18/2016.
 */
$(function(){
    var tasks = []

    var table = $("#todo-table");
    var task = $("#task");
    var date = $("#inDate");
    var index = 0;

    $("#add-btn").click(function(){
        add_entries();
    });

    function get()
    {
        $.ajax(
            {
                url: "http://localhost:3000",
                type: "GET",
                success: function(response){
                    tasks = response.data;
                    console.log("Initial get recieved");
                    console.log(tasks);
                    $("#todo-table tbody tr").remove();
                    initial_add();
                }
            }
        )
    };

    get();


    $("#checkAll").click(function(){
        if ($(this).prop("checked"))
        {
            for (var i = 0; i<tasks.length; i++)
            {
                tasks[i].status = true;
            }
        }
        else
        {
            for (var i = 0; i<tasks.length; i++)
            {
                tasks[i].status = false;
            }
        }
        if (tasks.length >= 0)
        {
            patch_it(0);
        }
        else
        {
            get();
        }
    });

    function initial_add(){
        for (var i = 0; i<tasks.length; i++){
            var s = tasks[i].task;
            var datestr = tasks[i].due;

            console.log(s);

            var td0 = $('<td>').append($('<input class="doneCheck" type="checkbox">'));
            var td1 = $('<td class="aTask">').html(s);
            var tdDate = $('<td class="dueField">').html(datestr);
            var td2 = $('<td>').append($('<button class="xButton">').html("x"));
            var tr = $('<tr>').attr("data-indexer", index).append(td0).append(td1).append(tdDate).append(td2);

            if (tasks[i].status == true)
            {
                tr.find(".doneCheck").prop("checked", true);
                tr.addClass("strike");
                tr.find(".xButton").prop("disabled", true);
            }

            table.append(tr);
        }
    };

    function add_entries(){
        console.log("button clicked");
        var s = task.val();
        var datestr = date.val();

        myEntry = {
            task : s,
            status : false,
            due : datestr,
        };
        tasks.push(myEntry);
        console.log(tasks);

        $.ajax(
            {
                url: "http://localhost:3000/",
                type: "post",
                data: myEntry,
                success: function(){
                    console.log("object sent succesfully");
                    get();
                }
            }
        )
    };

    var delfunc = function(){
        console.log($(this));
        var curIndex = $("tr").index($(this).parent().parent()) - 1;
        //$(this).parent().parent().remove();
        console.log(tasks);
        console.log(curIndex);
        var id = tasks[curIndex]._id;
        tasks.splice(curIndex, 1);
        console.log(tasks);


        $.ajax({
            url: "http://localhost:3000/" + id,
            type: "delete",
            success: function(){
                console.log("object deleted");
                get();
            }
        });
    };

    var crossfunc = function(){
        var currCheck = $(this);
        var parentRow = currCheck.parent().parent()
        var xbox = parentRow.find(".xButton");
        var curIndex = $("tr").index($(this).parent().parent()) - 1;
        var id = tasks[curIndex]._id;
        console.log(curIndex);
        console.log(tasks);
        console.log("current row: " + curIndex);

        tasks[curIndex].status = !tasks[curIndex].status;

        patch_it(curIndex);

    };

    function patch_it(curIndex){
        curObj = tasks[curIndex];
        $.ajax({
            dataType: 'json',
            url: "http://localhost:3000/" + curObj._id,
            type: "patch",
            data: {task: tasks},
            success: function()
            {
                get();
            }
        });
    };

    function updateTask(){
        var curIndex = $("tr").index($(this).parent()) - 1;
        curStr = $(this).html();
        var myIn = $("<input>");
        myIn.attr("value", curStr);
        $(this).replaceWith(myIn);
        myIn.focus();
        myIn.blur(function(){
            console.log("blurred");
            tasks[curIndex].task = myIn.val();
            console.log(myIn.val());
            patch_it(curIndex);
        })
    };

    function updateDate(){
        var curIndex = $("tr").index($(this).parent()) - 1;
        curStr = $(this).html();
        var myIn = $("<input>");
        myIn.attr("type", "date");
        myIn.attr("value", curStr);
        $(this).replaceWith(myIn);
        myIn.focus();
        myIn.blur(function(){
            console.log("blurred");
            tasks[curIndex].due = myIn.val();
            console.log(myIn.val());
            patch_it(curIndex);
        })
    };


    table.on("click", ".xButton", delfunc);
    table.on("click", ".doneCheck", crossfunc);
    table.on("click", ".aTask", updateTask);
    table.on("click", ".dueField", updateDate);
});